package org.uma.pab.Tarea4;

import org.junit.*;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by ajnebro on 9/3/16.
 */
public class PruebaListMockito {
	
  @Test
  public void pruebaLista() {
    // Creating the mock object
    List<String> list = mock(List.class) ;

    // Stubbing: defining the behavior
    when(list.get(0)).thenReturn("First") ;
    when(list.get(1)).thenReturn("Second") ;
    when(list.get(3)).thenReturn("Third") ;
    //when(list.get(2)).thenThrow(new RuntimeException()) ;

    // Using the mock object
    System.out.println(list.get(0));
    System.out.println(list.get(1));
    //System.out.println(list.get(2));
    System.out.println(list.get(3));

    // Verifying
    verify(list, times(1)).get(0) ;
    verify(list, never()).get(2) ;
    verify(list, times(1)).get(1);
    verify(list, times(1)).get(3);
    Mockito.doThrow(new RuntimeException()).when(list).get(2);
    
  }
  
  
	  
  
}
